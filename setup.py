from setuptools import setup, find_packages 


def parse_requirements(filename):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]


setup(name='jcpyserver',
      version='1.4',
      description='Package to manage APIs input/output with python, combining flask and sqlalchemy.',
      url='https://bitbucket.org/ShihHsuanChen/jcpyserver.git',
      author='ShihHsuanChen',
      author_email='jack072315@gmail.com',
      license='MIT',
      packages=find_packages(),
      install_requires=parse_requirements('requirements.txt'),
      zip_safe=False)
