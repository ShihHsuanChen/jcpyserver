#!/bin/bash
# assign environment image name
NAME=jcpyserver
# assign environment image version tag
TAG=v1.4
# assign python version
PYTHON_VERSION="python:3.7"
## 
SEARCH=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep "$NAME:$TAG")
if [ -z $SEARCH ];then
docker pull ${PYTHON_VERSION}
packages=$(echo $(cat ./requirements.txt))

docker build -t $NAME:$TAG - << EOF
FROM ${PYTHON_VERSION}
RUN apt update
RUN apt install -y vim
RUN pip install $packages
RUN pip install git+https://bitbucket.org/ShihHsuanChen/jcpyserver.git@${TAG}
EOF

else
    echo image "$NAME:$TAG" already exist
    docker images
fi
