from jcpyserver.request_parser import RequestParser
from jcpyserver.request_parser import set_log_path as set_request_log_path
from jcpyserver.status_handler import InvalidValueError

from jcpyserver.jcpyserver import set_log_path as set_response_log_path
from jcpyserver.jcpyserver import create_app, request, send_from_directory


#set_response_log_path('log/api.log', when="D", interval=1, backupCount=100)
#set_request_log_path('log/api.log', when="D", interval=1, backupCount=100)

app = create_app(__name__)


@app.route('/dynamic/<path>', nocache=True)
def dynamic_file(path):
    response = send_from_directory('./dynamic/', path)
    return response


@app.route('/static/<path>', nocache=False)
def static_file(path):
    response = send_from_directory('./static/', path)
    return response


@app.route('/foo', methods=['POST'])
def foo():
    response = dict()
    args = RequestParser(request)

    a = args.argument('a', vartype=int, notnone=True)
    response['a_square'] = a*a
    return response


@app.route('/foo1', methods=['POST'])
def foo1():
    args = RequestParser(request)

    a = args.argument('a', vartype=int, notnone=False)
    response = a*a*a
    return response


if __name__ == '__main__':
    app.run(host='127.0.0.1', port='5000')
