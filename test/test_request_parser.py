import json
import datetime
import traceback
from flask import Flask, request, url_for

from jcpyserver.status_handler import ApiReceiveError
from jcpyserver.status_handler import InvalidValueError

from jcpyserver.request_parser import RequestParser
from jcpyserver.request_parser import _parse_content_type
from jcpyserver.request_parser import set_log_path
from jcpyserver.request_parser import _get_request_log


app = Flask(__name__)

reqdata = {'a':3}

def test__parse_content_type1():
    ''' test content-type: application/json '''
    headers = {'content-type': "application/json"}
    with app.test_request_context('/foo', data=json.dumps(reqdata), headers=headers, method='POST'):
        output = _parse_content_type(request)
        assert len(output) == 2
        data = output[0]
        content_type = output[1]
        for key, val in reqdata.items():
            assert data.get(key) == val

def test__parse_content_type2():
    ''' test content-type: application/json with bad string format '''
    headers = {'content-type': "application/json"}
    with app.test_request_context('/foo', data="{'a', '3'}", headers=headers, method='POST'):
        try:
            output = _parse_content_type(request)
            assert False
        except ApiReceiveError:
            assert True
        except AssertionError:
            assert False

def test__parse_content_type3():
    ''' test content-type: application/x-www-form-urlencoded '''
    headers = {'content-type': "application/x-www-form-urlencoded"}
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        output = _parse_content_type(request)
        assert len(output) == 2
        data = output[0]
        content_type = output[1]
        for key, val in reqdata.items():
            assert data.get(key) == str(val)

def test__parse_content_type4():
    ''' test invalid content-type '''
    headers = {'content-type': "text"}
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        try:
            output = _parse_content_type(request)
            assert False
        except ApiReceiveError:
            assert True
        except AssertionError:
            assert False

def test__parse_content_type5():
    ''' test when content-type is absent in header '''
    headers = {}
    with app.test_request_context('/foo', data={}, headers=headers, method='POST'):
        try:
            output = _parse_content_type(request)
        except ApiReceiveError:
            raise
        except Exception as e:
            raise

reqdata = {
        'int1': 3,
        'int2': 1,
        'int3': 2,
        'int4': 3,
        'float': 3.333,
        'str1': 'b',
        'str2': '',
        'str3': 'sdf',
        'list1': json.dumps([1,2,3]),
        'list2': json.dumps([]),
        'dict1': json.dumps({'a':'a', 'b':'b'}),
        'bool1': '1',
        'bool2': 'True',
        'bool3': 'true',
        'bool4': True,
        'bool5': '0',
        'bool6': 'False',
        'bool7': 'false',
        'bool8': False,
        'datetime1': '2018-05-09',
        'datetime2': '20180509',
        'datetime3': '2018-05-09 23:58:01',
        'callable1': '1/2/3',
        'callable2': 'a/b/3',
        'iterable1': json.dumps([1,2,3]),
        'iterable2': 'False',
        'iterable3': '1/2/3',
        }
headers = {'content-type': "application/x-www-form-urlencoded"}
def test_req_format0():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('null1', vartype=int, notnone=False) is None

def test_req_format1():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        try:
            dumy = reqparse.argument('null2', vartype=int, notnone=True)
            assert False;
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format2():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('default', vartype=int, notnone=False, default=1) is None

def test_req_format3():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('default', vartype=int, notnone=True, default=1) == 1

def test_req_format4():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('int1', vartype=int) == 3

def test_req_format5():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('int2', vartype=int, validvaluelist=[1, 2]) == 1

def test_req_format6():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('int3', vartype=int, validvaluelist=[1, 2]) == 2

def test_req_format7():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        try:
            dumy = reqparse.argument('int4', vartype=int, validvaluelist=[1, 2])
            assert False
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format8():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('float', vartype=float) == 3.333
        assert reqparse.argument('str1') == 'b'
        try:
            dumy = reqparse.argument('str2', vartype=str, notempty=True)
            assert False;
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format9():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('str3', vartype=str) == 'sdf'

def test_req_format10():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert len(reqparse.argument('list1', vartype=list)) == 3

def test_req_format11():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        try:
            dumy = reqparse.argument('list2', vartype=list, notempty=True)
            assert False
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format11():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('bool1', vartype=bool)

def test_req_format12():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('bool2', vartype=bool)

def test_req_format13():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('bool3', vartype=bool)

def test_req_format14():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('bool4', vartype=bool)

def test_req_format15():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert not reqparse.argument('bool5', vartype=bool)

def test_req_format16():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert not reqparse.argument('bool6', vartype=bool)

def test_req_format17():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert not reqparse.argument('bool7', vartype=bool)

def test_req_format18():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert not reqparse.argument('bool8', vartype=bool)

def test_req_format19():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('datetime1', vartype=datetime.datetime).strftime('%Y-%m-%d') == '2018-05-09'

def test_req_format20():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        try:
            dumy = reqparse.argument('datetime2', vartype=datetime.datetime)
            assert False;
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format21():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('datetime3', vartype=datetime.datetime).strftime('%Y-%m-%d %H:%M:%S') == '2018-05-09 23:58:01'


def myFunc(string):
    arr = list()
    for x in string.split('/'):
        arr.append(int(x))
    return arr

def test_req_format22():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert isinstance(reqparse.argument('callable1', vartype=myFunc), list)

def test_req_format23():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        try:
            dumy = reqparse.argument('callable2', vartype=myFunc)
            assert False;
        except Exception as e:
            assert isinstance(e, ApiReceiveError)

def test_req_format24():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert isinstance(reqparse.argument('iterable1', vartype=(list, bool, myFunc)), list)

def test_req_format25():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('iterable2', vartype=(list, bool, myFunc)) == False

def test_req_format26():
    with app.test_request_context('/foo', data=reqdata, headers=headers, method='POST'):
        reqparse = RequestParser(request)
        assert isinstance(reqparse.argument('iterable3', vartype=(list, bool, myFunc)), list)

def test_content_type_json():
    json_header = {'content-type': "application/json"}
    with app.test_request_context('/foo', data=json.dumps({'a': [1,2,3]}), headers=json_header, method='POST'):
        reqparse = RequestParser(request)
        assert reqparse.argument('a', vartype=list)
