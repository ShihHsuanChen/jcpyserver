import traceback
from jcpyserver.status_handler import get_error_msg
from jcpyserver.status_handler import get_status_code
from jcpyserver.status_handler import Error
from jcpyserver.status_handler import ApiReceiveError
from jcpyserver.status_handler import AuthenticationError
from jcpyserver.status_handler import InvalidValueError
from jcpyserver.status_handler import DbOperationError


def test_get_error_msg():
    assert get_error_msg(212, 'db_r_reject', 'project/task', traceback.format_exc()) == '(212) db_r_reject: project/task\nNoneType: None\n'

def test_get_status_code1():
    stat = get_status_code()
    assert len(stat) == 3
    assert stat[0] == 100
    assert stat[1] == 'no_error'
    assert stat[2] == ''

def test_get_status_code2():
    stat = get_status_code(AssertionError())
    assert len(stat) == 3
    assert stat[0] == 900
    assert stat[1] == 'unknown_error'
    assert stat[2] == "<class 'AssertionError'>"

def test_get_status_code3():
    stat = get_status_code(Error())
    assert len(stat) == 3
    assert stat[0] == 900
    assert stat[1] == 'unknown_error'
    assert stat[2] == ''

def test_get_status_code4():
    stat = get_status_code(ApiReceiveError('method', 'method error'))
    assert len(stat) == 3
    assert stat[0] == 200
    assert stat[1] == 'method_error'
    assert stat[2] == 'method error'

def test_get_status_code5():
    stat = get_status_code(AuthenticationError('authentication', 'authentication error'))
    assert len(stat) == 3
    assert stat[0] == 300
    assert stat[1] == 'authentication_error'
    assert stat[2] == 'authentication error'

def test_get_status_code6():
    stat = get_status_code(InvalidValueError('invalid value error'))
    assert len(stat) == 3
    assert stat[0] == 220
    assert stat[1] == 'invalid_value'
    assert stat[2] == 'invalid value error'

def test_get_status_code7():
    stat = get_status_code(DbOperationError('delete', 'delete error'))
    assert len(stat) == 3
    assert stat[0] == 214
    assert stat[1] == 'db_d_reject'
    assert stat[2] == 'delete error'
