"""
test items:
    response:
        case: no error, check status, value 
        case: expected error raised, check status
        case: invalid api return format, catch
    cache:
        case: nocache=True, check header
        case: nocache=False, check header
"""
import pytest
import json

from .app import app


@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.app_context():
        client = app.test_client()
        yield client

def test_response_wrap1(client):
    response = client.post('/foo',
                           data={'a': 3},
                           content_type='application/x-www-form-urlencoded',
                           )
    res = response.get_data()
    data = json.loads(res)
    assert data.get('status') is not None
    assert data.get('status')[0] == 100
    assert data.get('a_square') == 9

def test_response_wrap2(client):
    response = client.post('/foo',
                           data={'b': 3},
                           content_type='application/x-www-form-urlencoded',
                           )
    res = response.get_data()
    data = json.loads(res)
    assert data.get('status') is not None
    assert data.get('status')[0] == 201
    assert data.get('a_square') is None

def test_response_wrap3(client):
    try:
        response = client.post('/foo1',
                               data={'a': 3},
                               content_type='application/x-www-form-urlencoded',
                               )
        assert False
    except AssertionError:
        assert True
    except Exception as e:
        assert isinstance(e, InvalidValueError)

def test_cache1(client):
    response = client.get('/dynamic/file.txt')
    assert response.headers['Cache-Control'] == 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'

def test_cache2(client):
    response = client.get('/static/file.txt')
    assert response.headers['Cache-Control'] != 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
