# jcpyserver
#### keywords
python3, flask, sqlalchemy, request parser
#### Properse
We attempt to put some simple usage of flask, sqlalchemy, and our pre-defined response format together in order to get rid of routines of building the same setting when a new small project is created. Simply says, we do all setting in a 'create_app' function so that users can just write the simplest flask syntax, and this package would deal with the error logging, response with error message, db session works and so on.
## version v1.0
### modules
- jcpyserver
- request_parser
- status_handler
#### jcpyserver
- **set_log_path**(log_path, when=None, interval=1, backCount=0)
	* A *function* to set the logging of response body.
	* args
    	* **log_path**:  path to write the log.
		* **when**:  see argument 'when' of logging.handlers.TimedRotatingFileHandler
		* **interval**:  see argument 'interval' of logging.handlers.TimedRotatingFileHandler
		* **backupCount**:  see argument 'backupCount' of logging.handlers.
		* [ref] https://docs.python.org/2.7/library/logging.handlers.html#timedrotatingfilehandler
	* reuturn
		* (None)
	* example
```
			# main.py
			from jcpyserver.jcpyserver import set_log_path as set_response_log_path
			
			set_response_log_path('log/api.log', when="D", interval=1, backupCount=100)
```

- **create_app**(name)
	* This *function* add the error message into the response body and then transform the data into json format string
	* args
		* **name**: name of app. As same as input argument of Flask.
		* [ref] https://flask.palletsprojects.com/en/1.1.x/api/
	* reuturn
		* (None)
	* example
```
			# main.py
			from jcpyserver.jcpyserver import create_app
			
			app = create_app(__name__)
			
			@app.route('/foo', methods=['POST'])
			def foo():
				response = dict()
				return response
			
			if __name__ == '__main__':
				app.run(host='127.0.0.1', port='5000') 
```
- other
	* request
	* send_from_directory
#### request_parser
- **set_log_path**(log_path, when=None, interval=1, backCount=0)
	* A *function* to set the logging of response body.
	* args
    	* **log_path**:  path to write the log.
		* **when**:  see argument 'when' of logging.handlers.TimedRotatingFileHandler
		* **interval**:  see argument 'interval' of logging.handlers.TimedRotatingFileHandler
		* **backupCount**:  see argument 'backupCount' of logging.handlers.
		* [ref] https://docs.python.org/2.7/library/logging.handlers.html#timedrotatingfilehandler
	* reuturn
		* (None)
	* example
```
			# main.py
			from jcpyserver.request_parser import set_log_path as set_request_log_path
			
			set_request_log_path('log/api.log', when="D", interval=1, backupCount=100)
```
- **RequestParser** (*class*)
	* A *class* to parse the request data, either json format or form-data, to the target format (ex: int/str/datatime.datetime)
	* methods
		* **RequestParser**(self, request)
			* constructor
			* args
				* **request**: as same as flask.request
			* return
				* (RequestParser object)
		* **argument**(self, name: str, vartype=str, validvaluelist=None, notnone: bool=True, notempty: bool=True, default=None)
			* To parse or cast a key of request data with specific name. User can assign a variable type, required or optional, and so on.
			* args
				* **name**: name of key
				* **vartype**: Target varibale type (default is str). Adaptable: *int*, *float*, *str*, *bool*, *datatime.datetime*, *list*, *dict*, and other castable types. Notice that in v1.0 '%Y-%m-%d' is the only vaild *datetime.datetime* format, ex: 2019-05-01. In the case of *bool* type, the input string can be either 'True', 'true', 'T', '1', or *True* for *True* case, 'False', 'false', 'F', '0', or *False* for *False* case.
				* **validvaluelist**: A list of valid input value (default is None, or not set). Notice that the value in *validvaluelist* should be compatable with *vartype*.
				* **notnone**: *True* for requird argument, *False* for optional argument. (default is *True*)
				* **notempty**: Can value of argument be empty or not (default is *True*). Case *True* requires a not-empty value, vice versa. Empty case: *vartype*=*list* or *dict*: [], {}; *vartype*=*str*: ''
				* **default**: default value if the argument is not given or the value is *None* (*null* of json). Notice that if *notnone* is *False*, it will never be assigned to the given default value.
			* return
				* **arg**: value of argument of the given **vartype*
		* example
```
			# main.py
			# This api foo return a statistic value of a list of number with a weight.
			# The statistic function can be 'average' (default), 'n1-norm', or 'n2-norm'
			# ex: calling /foo with post method using jquery post to calculate n2-norm of numbers [1,2,3] with weight 1.0
			#     /* js, jquery */
			#     var req = {
			#         numbers: JSON.stringify([1,2,3]),
			#         weight: 1.0,
			#         function: 'n2-norm'
			#     };
			# 	  $.post('http://127.0.0.1:5000/foo', req, function (data) {...})
			#     /* return value should be (1*1.0)^2 + (2*1.0)^2 + (3*1.0)^2 = 14 */
			
			from jcpyserver.request_parser import RequestParser
			from jcpyserver.jcpyserver import set_log_path as set_response_log_path
			from jcpyserver.jcpyserver import create_app, request

			app = create_app(__name__)

			@app.route('/foo', methods=['POST'])
			def foo():
    			args = RequestParser(request)

    			nums = args.argument('numbers', vartype=list, notnone=True)
				w = args.argument('weight', vartype=float, notnone=False)
				func = args.argument('function',
									 vartype=str,
									 validvalidlist=['n1-norm', 'n2-norm', 'average'],
									 notnone=True,
									 default='average')
				response = dict()
				stat = 0
				
				if w is None:
					w = 1
				
				if func == 'average':
					stat = sum(nums) / len(nums) * w
				elif func == 'n1-norm':
					stat = sum([(x*w) for x in nums])
				elif func == 'n2-norm':
					stat = sum([(x*w)**2 for x in nums])
					
    			response['stat'] = stat
    		return response

			if __name__ == '__main__':
    			app.run(host='127.0.0.1', port='5000')
```
### Install
```sh
user@foo:/your/dir$ pip install git+https://bitbucket.org/ShihHsuanChen/jcpyserver@v1.4
```
