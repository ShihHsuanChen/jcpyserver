import json
import logging
import datetime
import traceback

from flask import Flask, make_response, Response
from flask import request, send_from_directory
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from .status_handler import InvalidValueError, get_status_code

#from flask_sqlalchemy import SQLAlchemy


''' response logger setting '''

logger = logging.getLogger('apiOlog')
logger_cors = logging.getLogger('flask_cors')


def set_log_path(log_path, when=None, interval=1, backupCount=0):
    formatter = logging.Formatter('%(asctime)s #%(thread)d [%(levelname)s] %(message)s')
    if when is None:
        file_handler = logging.FileHandler(log_path, 'a', 'utf-8')
    else:
        file_handler = logging.handlers.TimedRotatingFileHandler(
            log_path,
            encoding='utf-8',
            when=when,
            interval=interval,
            backupCount=backupCount
            )
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(logging.INFO)
    logger_cors.addHandler(file_handler)
    logger.setLevel(logging.DEBUG)


def create_app(name):
    app = Flask(name)
    CORS(app)
    #app.config['SQLALCHEMY_BINDS'] = SQLALCHEMY_BINDS
    
    def _nocache(response):
        response.headers['Last-Modified'] = datetime.datetime.now()
        response.headers['Cache-Control'] = 'no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0'
        response.headers['Pragma'] = 'no-cache'
        response.headers['Expires'] = '-1'
        return response

    @app.errorhandler(Exception)
    def handle_error(e):
        status = get_status_code(e)
        res_json = json.dumps({'status': status}, ensure_ascii=False)
        response = make_response(res_json)
        return response

    app._route = app.route

    def route(rule, **options):
        manual_options = ['nocache']
        nocache = options.get('nocache')

        opts = {k:v for k, v in options.items() if k not in manual_options}

        def decorator(api):
            opts.update({'endpoint': api.__name__})

            @app._route(rule, **opts)
            def _inner_decorator(*args, **kwargs):
                res = api(*args, **kwargs)
                try:
                    if not isinstance(res, Response):
                        if res is None:
                            res = dict()
                        elif not isinstance(res, dict):
                            raise InvalidValueError('API return should be a dict')
                        
                        res['status'] = get_status_code()
                        res_json = json.dumps(res, ensure_ascii=False)
                        logger.info('response ' + res_json[:200])
                        response = make_response(res_json)
                    else:
                        # static file go here
                        response = res
                except InvalidValueError:
                    raise
                except Exception as e:
                    print(traceback.format_exc())
                    pass

                if nocache:
                    response = _nocache(response)

                return response

            return _inner_decorator

        return decorator

    app.route = route
    return app
