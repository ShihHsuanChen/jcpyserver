import traceback
import logging
import logging.handlers
from sys import stdout
from sys import exc_info


''' error logger setting '''

formatter_print = logging.Formatter('%(asctime)s #%(thread)d \033[31m[%(levelname)s]\033[0m %(message)s')
console_handler = logging.StreamHandler(stdout)
console_handler.setFormatter(formatter_print)

logger = logging.getLogger('funcErrlog')
logger.addHandler(console_handler)
logger.setLevel(logging.INFO)


def set_log_path(log_path, when=None, interval=1, backupCount=0):
    formatter = logging.Formatter('%(asctime)s #%(thread)d [%(levelname)s] %(message)s')
    if when is None:
        file_handler = logging.FileHandler(log_path, 'a', 'utf-8')
    else:
        file_handler = logging.handlers.TimedRotatingFileHandler(
            log_path,
            encoding='utf-8',
            when=when,
            interval=interval,
            backupCount=backupCount
            )
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(logging.INFO)


def get_error_msg(code, string, message, tb):
    msg = f'({code}) {string}: {message}\n{tb}'
    return msg


'''classes for error handling'''


class Error(Exception):
    '''Base class for exceptions'''
    def __init__(self, code=900, string='unknown_error', msg=''):
        super(Error, self).__init__()
        self.code = code
        self.string = string
        self.msg = msg

    def __str__(self):
        return f'\033[31mError({self.code}) {self.string}: {self.msg}\033[0m'


class DbOperationError(Error):
    '''Raise when disconnect/create/read/update/delete reject by db'''
    code_dict = {
        'disconnect': 210,
        'create': 211,
        'read': 212,
        'update': 213,
        'delete': 214
        }
    string_dict = {
        'disconnect': 'db_disconnect',
        'create': 'db_c_reject',
        'read': 'db_r_reject',
        'update': 'db_u_reject',
        'delete': 'db_d_reject'
        }

    def __init__(self, err_type, msg=''):

        assert (err_type in self.code_dict), f'err_type "{err_type}" not found'
        code = self.code_dict[err_type]
        string = self.string_dict[err_type]
        logger.error(get_error_msg(code, string, msg, traceback.format_exc()))

        super(DbOperationError, self).__init__(
            code=code,
            string=string,
            msg=msg
            )


class ApiReceiveError(Error):
    '''Raise when either method/argument/value is wrong'''
    code_dict = {
        'method': 200,
        'argument': 201,
        'value': 202,
        'content_type': 203,
        'data_format': 204
        }
    string_dict = {
        'method': 'method_error',
        'argument': 'argument_error',
        'value': 'value_error',
        'content_type': 'content_type_error',
        'data_format': 'data_format_error'
        }

    def __init__(self, err_type, msg=''):

        assert (err_type in self.code_dict), f'err_type "{err_type}" not found'
        code = self.code_dict[err_type]
        string = self.string_dict[err_type]
        logger.error(get_error_msg(code, string, msg, traceback.format_exc()))

        super(ApiReceiveError, self).__init__(
            code=code,
            string=string,
            msg=msg
            )


class AuthenticationError(Error):
    '''Raise when authentication/account/password/session is wrong'''
    code_dict = {
        'authentication': 300,
        'account': 301,
        'password': 302,
        'session': 303
        }
    string_dict = {
        'authentication': 'authentication_error',
        'account': 'unknown_user',
        'password': 'password_error',
        'session': 'offline'
        }

    def __init__(self, err_type, msg=''):

        assert (err_type in self.code_dict), f'err_type "{err_type}" not found'
        code = self.code_dict[err_type]
        string = self.string_dict[err_type]

        super(AuthenticationError, self).__init__(
            code=code,
            string=string,
            msg=msg
            )


class InvalidValueError(Error):

    '''Raise when variable value is invalid'''

    def __init__(self, msg=''):

        code = 220
        string = 'invalid_value'
        logger.error(get_error_msg(code, string, msg, traceback.format_exc()))

        super(InvalidValueError, self).__init__(
            code=code,
            string=string,
            msg=msg
            )


def get_status_code(exception=None):
    '''Only for api response'''
    if exception is None:
        return [100, 'no_error', '']

    if isinstance(exception, (Error,
                              DbOperationError,
                              ApiReceiveError,
                              AuthenticationError,
                              InvalidValueError)):
        if exception.code != 303:
            pass
    else:
        code = 900
        string = 'unknown_error'
        msg = f'{type(exception)}'
        logger.error(get_error_msg(code, string, msg, traceback.format_exc()))
        return [code, string, msg]

    return [exception.code, exception.string, exception.msg]

