import json
import datetime
import logging
import logging.handlers
import traceback

from urllib import parse
from typing import Union, Callable

from .status_handler import ApiReceiveError
from .status_handler import InvalidValueError


# logger setting
logger = logging.getLogger('apiIlog')


def _parse_content_type(req):
    """ Responds to an HTTP request using data from the request body parsed
    according to the "content-type" header.
    Args:
        req (flask.Request): The request object.
        <http://flask.pocoo.org/docs/1.0/api/#flask.Request>
    Returns:
        data: the dict type data with key-value pair
        content_type: content-type in request header
    """
    data = dict()
    content_type = 'unknown'

    try:
        content_type = req.headers['content-type']
        if 'application/json' in content_type:
            try:
                data = req.json
            except Exception as e:
                raise ApiReceiveError('data_format', 'Input data is not json')

        elif 'application/x-www-form-urlencoded' in content_type:
            data = req.form.to_dict()
        elif 'multipart/form-data' in content_type:
            data = req.form.to_dict()
        else:
            raise ApiReceiveError('content_type', f'content type: {content_type} is not supported.')
        if not isinstance(data, dict):
            raise ApiReceiveError('argument', f'data in request body should be parsed by key-value format (dict-type) not {type(data)}')
    except ApiReceiveError:
        raise
    except Exception as e:
        try:
            if req.is_json:
                try:
                    data = req.json
                    content_type += ': parse by json'
                except Exception as e:
                    raise ApiReceiveError('data_format', 'Input data is not json')
            else:
                data = req.form.to_dict()
                content_type += ': parse by form'
        except Exception as e:
            raise
    return data, content_type


def set_log_path(log_path, when=None, interval=1, backupCount=0):
    formatter = logging.Formatter('%(asctime)s #%(thread)d [%(levelname)s] %(message)s')
    if when is None:
        file_handler = logging.FileHandler(log_path, 'a', 'utf-8')
    else:
        file_handler = logging.handlers.TimedRotatingFileHandler(
            log_path,
            encoding='utf-8',
            when=when,
            interval=interval,
            backupCount=backupCount
            )
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.setLevel(logging.INFO)


def _get_request_log(req, data, content_type):
    url = req.url
    addr = req.remote_addr
    json_data = json.dumps(data, ensure_ascii=False)
    return f'from {addr} request {url} with [{content_type}] {json_data}'


class RequestParser():

    def __init__(self, request):
        self.request = request
        self.data, self.content_type = _parse_content_type(request)
        # logging
        logger.info(_get_request_log(request, self.data, self.content_type))

    def argument(
            self,
            name: str,
            vartype: Union[Callable, list, tuple, set, type]=str,
            validvaluelist: list = None,
            notnone: bool = True,
            notempty: bool = True,
            default=None
            ):
        arg = self.data.get(name)

        polymorphism = False

        if isinstance(vartype, (tuple, list, set)):
            polymorphism = True
            for x in vartype:
                if (not isinstance(x, type)) and (not callable(x)):
                    raise InvalidValueError(f'Error: type of elements of vartype should be either "type" or other callable functions')
            vartype_list = list(vartype)
        elif isinstance(vartype, type):
            vartype_list = [vartype]
        elif callable(vartype):
            vartype_list = [vartype]
        else:
            raise InvalidValueError(f'Error: type(vartype) should be either "type" or "tuple/list/set" or other callable functions')

        type_name_list = [x.__name__ for x in vartype_list]

        if default is not None:
            if not isinstance(default, tuple(vartype_list)):
                if polymorphism:
                    raise InvalidValueError(f'Error: type of default is not in {type_name_list}. type(default):{type(default)}')
                else:
                    raise InvalidValueError(f'Error: type of default is not {vartype_list[0].__name__}. type(default):{type(default)}')
            if notempty:
                if ((str in vartype_list) and (default == '')) or \
                   (((list in vartype_list) or (dict in vartype_list)) and (len(default) == 0)):
                    raise InvalidValueError(f'default value cannot be empty while notempty=True')
        if notnone:
            if (arg is None) and (default is None):
                raise ApiReceiveError('argument', name)
            elif (arg is None) and (default is not None):
                arg = default

        if validvaluelist is not None:
            if not isinstance(validvaluelist, list):
                raise InvalidValueError('argument: validvaluelist in function ApiParser.argument should be a list')
            for x in validvaluelist:
                if not isinstance(x, tuple(vartype_list)):
                    if polymorphism:
                        raise InvalidValueError(f'Error: type of default is not in {type_name_list}. type(default):{type(default)}')
                    else:
                        raise InvalidValueError(f'type of {x} in validvaluelist is not {vartype_list[0].__name__}. type({x}):{type(x)}')

        if arg is not None:
            # check type
            error_collection = list()
            for vartype in vartype_list:
                try:
                    if vartype is bool:
                        if arg in ['True', 'true', 'T', '1', True]:
                            arg = True
                        elif arg in ['False', 'false', 'F', '0', False]:
                            arg = False
                        else:
                            raise ApiReceiveError('value', f'input argument {name} should be 1/0 or True/False')
                    elif vartype is datetime.datetime:
                        try:
                            try:
                                arg = datetime.datetime.strptime(arg, '%Y-%m-%d %H:%M:%S')
                            except:
                                arg = datetime.datetime.strptime(arg, '%Y-%m-%d')
                        except Exception:
                            raise ApiReceiveError('value', f'input argument {name} should be in either format YYYY-mm-dd or YYYY-mm-dd HH:MM:SS')
                    elif vartype not in [list, dict]:
                        try:
                            arg = vartype(arg)
                        except Exception:
                            raise ApiReceiveError('value', f'input argument {name} should be a {vartype.__name__}')
                        if (vartype is str) and (arg == '') and notempty:
                            raise ApiReceiveError('value', f'input argument {name} cannot be \'\'')
                        if validvaluelist is not None:
                            if arg not in validvaluelist:
                                raise ApiReceiveError('value', f'value of argument {name} is invalid. {arg} is not in {validvaluelist}.')
                    elif not isinstance(vartype, type) and callable(vartype):
                        try:
                            arg = vartype(arg)
                        except Exception:
                            print(traceback.format_exc())
                            raise ApiReceiveError('value', f'input argument {name} cannot be parsed by {vartype.__name__}')
                    else:
                        if not isinstance(arg, vartype) and isinstance(arg, str):
                            try:
                                arg = json.loads(arg)
                            except Exception:
                                print(traceback.format_exc())
                                raise ApiReceiveError('value', f'input argument {name} should be a {vartype.__name__}')

                        if not isinstance(arg, vartype):
                            raise ApiReceiveError('value', f'input argument {name} should be a {vartype.__name__}')

                        if (len(arg) == 0) and notempty:
                            raise ApiReceiveError('value', f'input argument {name} cannot be empty {vartype.__name__}')
                    break
                except Exception as e:
                    error_collection.append(e)
            else:
                if polymorphism:
                    msglist = [e.msg for e in error_collection]
                    raise ApiReceiveError('value', '/'.join(msglist))
                else:
                    raise error_collection[0]

        return arg
